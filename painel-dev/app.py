import streamlit as st
from multiapp import MultiApp
from apps import home,ips, diagnostico #, visaogeral# import your app modules here

app = MultiApp()

st.set_page_config(layout='wide',page_title="FOCO")

# Add all your application here
app.add_app("Home", home.app)
app.add_app("IPS", ips.app)
#app.add_app("Dado", heatmap.app)
app.add_app("FOCO",diagnostico.app)
# The main app
app.run()
