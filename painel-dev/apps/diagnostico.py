from cProfile import label
from email.policy import default
from select import select
from tkinter import font
from typing import Sequence
from matplotlib import container
import pandas as pd
import leafmap.foliumap as leafmap
import leafmap.colormaps as cm
import matplotlib.pyplot as plt
import numpy as np
import streamlit as st
import geopandas as gpd
import warnings
from math import radians, cos, sin, asin, sqrt


def app():
    

    mapa = st.sidebar.radio("COMPONENTES",(
    "Nutrição e cuidados médicos básicos",
    "Água e saneamento",
    "Moradia",
    "Segurança pessoal",
    "Acesso ao conhecimento básico",
    "Acesso à informação",
    "Saúde e bem-estar",
    "Qualidade do meio ambiente",
    "Direitos individuais",
    "Liberdades individuais",
    "Tolerância e inclusão",
    "Acesso á educação superior"))
    
    
    with st.container():
        col1, col2= st.columns([1,3])

        with col1:
            st.write('''
            # CHOQUE 
            ''')
        with col2:
            input_ips = st.slider('Insira a itensidade do choque desejado',min_value=1,max_value=12)

    if mapa == "Nutrição e cuidados médicos básicos":
        
        with st.container():
            m = leafmap.Map(google_map='ROADMAP',center=[0,0],zoom=2)
      
        
            if input_ips == 1:
            # in_geojson = ipsGeral.to_file('/home/leolo/Área de Trabalho/labfaz/projetos/foco/.venv/foco/data/ipsGeral.json', driver="GeoJSON")
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_1/ips_cuidadosMedicosBasicos.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

        
         
            elif input_ips == 2:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_2/ips_cuidadosMedicosBasicos.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)
            elif input_ips == 3:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_3/ips_cuidadosMedicosBasicos.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)
    
            elif input_ips == 4:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_4/ips_cuidadosMedicosBasicos.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 5:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_5/ips_cuidadosMedicosBasicos.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 6:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_6/ips_cuidadosMedicosBasicos.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 7:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_7/ips_cuidadosMedicosBasicos.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 8:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_8/ips_cuidadosMedicosBasicos.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 9:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_9/ips_cuidadosMedicosBasicos.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 10:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_10/ips_cuidadosMedicosBasicos.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 11:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_11/ips_cuidadosMedicosBasicos.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 12:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_12/ips_cuidadosMedicosBasicos.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)
        
    if mapa == "Água e saneamento":

        with st.container():    
            m = leafmap.Map(google_maps='ROADMAP',center=[0,0],zoom=2)
      
        
            if input_ips == 1:
            # in_geojson = ipsGeral.to_file('/home/leolo/Área de Trabalho/labfaz/projetos/foco/.venv/foco/data/ipsGeral.json', driver="GeoJSON")
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_1/ips_aguaEsaneamento.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)
            
            elif input_ips == 2:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_2/ips_aguaEsaneamento.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 3:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_3/ips_aguaEsaneamento.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 4:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_4/ips_aguaEsaneamento.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 5:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_5/ips_aguaEsaneamento.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 6:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_6/ips_aguaEsaneamento.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 7:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_7/ips_aguaEsaneamento.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 8:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_8/ips_aguaEsaneamento.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 9:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_9/ips_aguaEsaneamento.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 10:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_10/ips_aguaEsaneamento.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 11:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_11/ips_aguaEsaneamento.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 12:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_12/ips_aguaEsaneamento.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)


    if mapa == "Moradia":

        with st.container():
            m = leafmap.Map(google_maps='ROADMAP',center=[0,0],zoom=2)

            if input_ips == 1:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_1/ips_moradia.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 2:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_2/ips_moradia.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 3:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_3/ips_moradia.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 4:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_4/ips_moradia.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 5:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_5/ips_moradia.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 6:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_6/ips_moradia.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 7:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_7/ips_moradia.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 8:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_8/ips_moradia.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 9:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_9/ips_moradia.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 10:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_10/ips_moradia.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 11:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_11/ips_moradia.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 12:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_12/ips_moradia.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)


    if mapa == "Segurança pessoal":
    
        with st.container():
            m = leafmap.Map(google_maps='ROADMAP',center=[0,0],zoom=2)


            if input_ips == 1:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_1/ips_segurancaPessoal.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 2:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_2/ips_segurancaPessoal.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 3:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_3/ips_segurancaPessoal.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 4:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_4/ips_segurancaPessoal.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 5:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_5/ips_segurancaPessoal.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 6:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_6/ips_segurancaPessoal.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 7:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_7/ips_segurancaPessoal.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 8:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_8/ips_segurancaPessoal.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 9:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_9/ips_segurancaPessoal.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 10:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_10/ips_segurancaPessoal.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 11:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_11/ips_segurancaPessoal.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 12:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_12/ips_segurancaPessoal.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)


    if mapa == "Acesso ao conhecimento básico":
    
        with st.container():
            m = leafmap.Map(google_maps='ROADMAP',center=[0,0],zoom=2)

            if input_ips == 1:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_1/ips_acessoAoConhecimentoBasico.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 2:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_2/ips_acessoAoConhecimentoBasico.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 3:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_3/ips_segurancaPessoal.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 4:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_4/ips_acessoAoConhecimentoBasico.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 5:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_5/ips_segurancaPessoal.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 6:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_6/ips_segurancaPessoal.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 7:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_7/ips_segurancaPessoal.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 8:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_8/ips_segurancaPessoal.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 9:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_9/ips_segurancaPessoal.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 10:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_10/ips_segurancaPessoal.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 11:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_11/ips_segurancaPessoal.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 12:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_12/ips_segurancaPessoal.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)    


    if mapa == "Acesso à informação":
    
        with st.container():
            m = leafmap.Map(google_maps='ROADMAP',center=[0,0],zoom=2)

            if input_ips == 1:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_1/ips_acessoInformacao.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 2:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_2/ips_acessoInformacao.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 3:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_3/ips_acessoInformacao.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 4:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_4/ips_acessoInformacao.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 5:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_5/ips_acessoInformacao.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 6:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_6/ips_acessoInformacao.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 7:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_7/ips_acessoInformacao.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 8:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_8/ips_acessoInformacao.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 9:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_9/ips_acessoInformacao.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 10:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_10/ips_acessoInformacao.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 11:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_11/ips_acessoInformacao.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 12:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_12/ips_acessoInformacao.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)  

    if mapa == "Saúde e bem-estar":
    
        with st.container():
            m = leafmap.Map(google_maps='ROADMAP',center=[0,0],zoom=2)
       
            if input_ips == 1:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_1/ips_saudeBemEstar.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 2:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_2/ips_saudeBemEstar.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 3:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_3/ips_saudeBemEstar.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 4:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_4/ips_saudeBemEstar.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 5:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_5/ips_saudeBemEstar.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 6:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_6/ips_saudeBemEstar.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 7:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_7/ips_saudeBemEstar.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 8:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_8/ips_saudeBemEstar.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 9:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_9/ips_saudeBemEstar.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 10:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_10/ips_saudeBemEstar.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 11:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_11/ips_saudeBemEstar.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 12:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_12/ips_saudeBemEstar.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)  

    if mapa == "Qualidade do meio ambiente":
    
        with st.container():
            m = leafmap.Map(google_maps='ROADMAP',center=[0,0],zoom=2)
            if input_ips == 1:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_1/ips_qualMeioAmbiente.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 2:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_2/ips_qualMeioAmbiente.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 3:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_3/ips_qualMeioAmbiente.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 4:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_4/ips_qualMeioAmbiente.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 5:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_5/ips_qualMeioAmbiente.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 6:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_6/ips_qualMeioAmbiente.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 7:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_7/ips_qualMeioAmbiente.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 8:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_8/ips_qualMeioAmbiente.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 9:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_9/ips_qualMeioAmbiente.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 10:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_10/ips_qualMeioAmbiente.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 11:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_11/ips_qualMeioAmbiente.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 12:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_12/ips_qualMeioAmbiente.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)  
    if mapa == "Direitos individuais":
    
        with st.container():
            m = leafmap.Map(google_maps='ROADMAP',center=[0,0],zoom=2)
            if input_ips == 1:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_1/ips_direitosInividuais.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 2:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_2/ips_direitosInividuais.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 3:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_3/ips_direitosInividuais.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 4:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_4/ips_direitosInividuais.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 5:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_5/ips_direitosInividuais.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 6:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_6/ips_direitosInividuais.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 7:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_7/ips_direitosInividuais.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 8:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_8/ips_direitosInividuais.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 9:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_9/ips_direitosInividuais.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 10:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_10/ips_direitosInividuais.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 11:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_11/ips_direitosInividuais.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 12:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_12/ips_direitosInividuais.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)
    if mapa == "Liberdades individuais":
    
        with st.container():
            m = leafmap.Map(google_maps='ROADMAP',center=[0,0],zoom=2)
            if input_ips == 1:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_1/ips_liberdadesIndividuais.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 2:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_2/ips_liberdadesIndividuais.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 3:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_3/ips_liberdadesIndividuais.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 4:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_4/ips_liberdadesIndividuais.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 5:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_5/ips_liberdadesIndividuais.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 6:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_6/ips_liberdadesIndividuais.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 7:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_7/ips_liberdadesIndividuais.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 8:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_8/ips_liberdadesIndividuais.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 9:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_9/ips_liberdadesIndividuais.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 10:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_10/ips_liberdadesIndividuais.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 11:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_11/ips_liberdadesIndividuais.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 12:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_12/ips_liberdadesIndividuais.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

    if mapa == "Tolerância e inclusão":
    
        with st.container():
            m = leafmap.Map(google_maps='ROADMAP',center=[0,0],zoom=2)
            if input_ips == 1:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_1/ips_toleranciaInclusao.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 2:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_2/ips_toleranciaInclusao.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 3:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_3/ips_toleranciaInclusao.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 4:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_4/ips_toleranciaInclusao.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 5:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_5/ips_toleranciaInclusao.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 6:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_6/ips_toleranciaInclusao.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 7:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_7/ips_toleranciaInclusao.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 8:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_8/ips_toleranciaInclusao.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 9:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_9/ips_toleranciaInclusao.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 10:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_10/ips_toleranciaInclusao.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 11:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_11/ips_toleranciaInclusao.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 12:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_12/ips_toleranciaInclusao.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

    if mapa == "Acesso á educação superior":
    
        with st.container():
            m = leafmap.Map(google_maps='ROADMAP',center=[0,0],zoom=2)
            if input_ips == 1:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_1/ips_acessoEducacaoSuperior.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 2:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_2/ips_acessoEducacaoSuperior.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 3:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_3/ips_acessoEducacaoSuperior.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 4:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_4/ips_acessoEducacaoSuperior.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 5:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_5/ips_acessoEducacaoSuperior.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 6:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_6/ips_acessoEducacaoSuperior.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 7:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_7/ips_acessoEducacaoSuperior.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 8:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_8/ips_acessoEducacaoSuperior.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 9:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_9/ips_acessoEducacaoSuperior.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 10:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_10/ips_acessoEducacaoSuperior.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 11:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_11/ips_acessoEducacaoSuperior.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)

            elif input_ips == 12:
                in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/foco_data/ips_12/ips_acessoEducacaoSuperior.json"
                m.add_data(in_geojson,column=mapa,scheme='EqualInterval',k=31, cmap='inferno',legend_title='IPS RA')
                    
                m.to_streamlit(height=1000, width=2000)
