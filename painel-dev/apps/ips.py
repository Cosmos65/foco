

from select import select
from tkinter import font
from matplotlib import container
import pandas as pd
import leafmap.foliumap as leafmap
import leafmap.colormaps as cm
import matplotlib.pyplot as plt
import numpy as np

import streamlit as st



def app():
    #def load_data(df):
    #    dados = pd.read_csv(df)
    #    return dados
    mapa = st.sidebar.radio("COMPONENTES",(
    "Nutrição e cuidados médicos básicos",
    "Água e Saneamento",
    "Moradia",
    "Segurança Pessoal",
    "Acesso ao Conhecimento Básico",
    "Acesso à Informação",
    "Saúde e Bem-Estar",
    "Qualidade do Meio Ambiente",
    "Direitos Individuais",
    "Liberdades Individuais",
    "Tolerância e Inclusão",
    "Acesso á Educação Superior"))


    
    if mapa == "Nutrição e cuidados médicos básicos":
        
        with st.container():
            m = leafmap.Map(google_maps='ROADMAP',center=[0,0],zoom=2)
            
            in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/ips_cuidadosMedicosBasicos.json"
            m.add_data(in_geojson,column='Nutrição e cuidados médicos básicos',scheme='EqualInterval',k=33,cmap='inferno',legend_title='IPS RA')
            
            m.to_streamlit(height=1000, width=2000)
        with st.container():
            st.title("Indicadores")

            #st = st.st("Veja os indicadores")
            
            visIndicadores = st.radio('Escolha a forma de visualização dos indicadores',('5 Piores','Piores Indicadores'))

            if visIndicadores == 'Piores Indicadores':
                df = pd.read_csv('https://gitlab.com/Cosmos65/foco/-/raw/main/data/indicadores/nutricao_cuidados_medicos_basicos.csv',sep=";")
            
                st.table(df.style.highlight_max(color="red"))

            elif visIndicadores == '5 Piores':
                df = pd.read_csv('https://gitlab.com/Cosmos65/foco/-/raw/main/data/indicadores/nutricao_cuidados_medicos_basicos.csv',sep=";")
                
                col1, col2, col3, col4 = st.columns(4)

                with col1:
                    mortalidade_na_infancia = df[['Região \nAdministrativa','Mortalidade na infância']].sort_values(by='Mortalidade na infância').tail()

                    st.table(mortalidade_na_infancia)
                with col2:
                    baixo_peso_ao_nascer = df[['Região \nAdministrativa','Baixo peso ao nascer']].sort_values(by='Baixo peso ao nascer').tail()
                    
                    st.table(baixo_peso_ao_nascer)
                with col3:
                    mortalidade_materna = df[['Região \nAdministrativa','Mortalidade materna']].sort_values(by='Mortalidade materna').tail()

                    st.table(mortalidade_materna)

                with col4:
                    internacoes_criseRespAguda = df[['Região \nAdministrativa','Internações infantis por crise respiratória aguda']].sort_values(by='Internações infantis por crise respiratória aguda').tail()

                    st.table(internacoes_criseRespAguda)
                    
                    


                


    elif mapa == "Água e Saneamento":
        m = leafmap.Map(google_maps='ROADMAP',center=[0,0],zoom=2)

        in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/ips_aguaEsaneamento.json"
        m.add_data(in_geojson,column='Água e saneamento',scheme='EqualInterval',k=33, cmap='inferno',legend_title='IPS RA')

        m.to_streamlit(height=1000, width=2000)
        with st.container():
            st.title("Indicadores")

            #st = st.st("Veja os indicadores")
            
            visIndicadores = st.radio('Escolha a forma de visualização dos indicadores',('5 Piores','Piores Indicadores'))

            if visIndicadores == 'Piores Indicadores':
                df = pd.read_csv('https://gitlab.com/Cosmos65/foco/-/raw/main/data/indicadores/agua_saneamento.csv',sep=";")
            
                st.table(df.style.highlight_max(color="red"))

            elif visIndicadores == '5 Piores':
                df = pd.read_csv('https://gitlab.com/Cosmos65/foco/-/raw/main/data/indicadores/agua_saneamento.csv',sep=";")
                
                col1, col2, col3 = st.columns(3)

                with col1:
                    mortalidade_na_infancia = df[['Região \nAdministrativa','Acesso à água canalizada']].sort_values(by='Acesso à água canalizada').tail()

                    st.table(mortalidade_na_infancia)
                with col2:
                    baixo_peso_ao_nascer = df[['Região \nAdministrativa','Acesso a esgotamento sanitário']].sort_values(by='Acesso a esgotamento sanitário').tail()
                    
                    st.table(baixo_peso_ao_nascer)
                with col3:
                    mortalidade_materna = df[['Região \nAdministrativa','Acesso a banheiro']].sort_values(by='Acesso a banheiro').tail()

                    st.table(mortalidade_materna)

                
    elif mapa == "Moradia":
        m = leafmap.Map(google_maps='ROADMAP',center=[0,0],zoom=2)

        in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/ips_moradia.json"
        m.add_data(in_geojson,column='Moradia',scheme='EqualInterval',k=33, cmap='inferno',legend_title='IPS RA')

        m.to_streamlit(height=1000, width=2000)
        with st.container():
            st.title("Indicadores")

            #st = st.st("Veja os indicadores")
            
            visIndicadores = st.radio('Escolha a forma de visualização dos indicadores',('5 Piores','Piores Indicadores'))

            if visIndicadores == 'Piores Indicadores':
                df = pd.read_csv('https://gitlab.com/Cosmos65/foco/-/raw/main/data/indicadores/moradia.csv',sep=";")
            
                st.table(df.style.highlight_max(color="red"))

            elif visIndicadores == '5 Piores':
                df = pd.read_csv('https://gitlab.com/Cosmos65/foco/-/raw/main/data/indicadores/moradia.csv',sep=";")
                
                col1, col2, col3= st.columns(3)

                with col1:
                    mortalidade_na_infancia = df[['Região \nAdministrativa','População vivendo em Favelas não-urbanizadas']].sort_values(by='População vivendo em Favelas não-urbanizadas').tail()

                    st.table(mortalidade_na_infancia)
                with col2:
                    baixo_peso_ao_nascer = df[['Região \nAdministrativa','Acesso à energia elétrica']].sort_values(by='Acesso à energia elétrica').tail()
                    
                    st.table(baixo_peso_ao_nascer)
                with col3:
                    mortalidade_materna = df[['Região \nAdministrativa','Adensamento habitacional excessivo']].sort_values(by='Adensamento habitacional excessivo').tail()

                    st.table(mortalidade_materna)

               
    elif mapa == "Segurança Pessoal":
        m = leafmap.Map(google_maps='ROADMAP',center=[0,0],zoom=2)

        in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/ips_segurancaPessoal.json"
        m.add_data(in_geojson,column='Segurança pessoal',scheme='EqualInterval',k=33, cmap='inferno',legend_title='IPS RA')

        m.to_streamlit(height=1000, width=2000)
        with st.container():
            st.title("Indicadores")

            #st = st.st("Veja os indicadores")
            
            visIndicadores = st.radio('Escolha a forma de visualização dos indicadores',('5 Piores','Piores Indicadores'))

            if visIndicadores == 'Piores Indicadores':
                df = pd.read_csv('https://gitlab.com/Cosmos65/foco/-/raw/main/data/indicadores/seguranca_pessoal.csv',sep=";")
            
                st.table(df.style.highlight_max(color="red"))

            elif visIndicadores == '5 Piores':
                df = pd.read_csv('https://gitlab.com/Cosmos65/foco/-/raw/main/data/indicadores/seguranca_pessoal.csv',sep=";")
                
                col1, col2 = st.columns(2)

                with col1:
                    mortalidade_na_infancia = df[['Região \nAdministrativa','Taxa de homicídios']].sort_values(by='Taxa de homicídios').tail()

                    st.table(mortalidade_na_infancia)
                with col2:
                    baixo_peso_ao_nascer = df[['Região \nAdministrativa','Roubos de rua']].sort_values(by='Roubos de rua').tail()
                    
                    st.table(baixo_peso_ao_nascer)
                
    elif mapa == "Acesso ao Conhecimento Básico":
        m = leafmap.Map(google_maps='ROADMAP',center=[0,0],zoom=2)

        in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/ips_acessoAoConhecimentoBasico.json"
        m.add_data(in_geojson,column='Acesso ao conhecimento básico',scheme='EqualInterval',k=33, cmap='inferno',legend_title='IPS RA')

        m.to_streamlit(height=1000, width=2000)
        with st.container():
            st.title("Indicadores")

            #st = st.st("Veja os indicadores")
            
            visIndicadores = st.radio('Escolha a forma de visualização dos indicadores',('5 Piores','Piores Indicadores'))

            if visIndicadores == 'Piores Indicadores':
                df = pd.read_csv('https://gitlab.com/Cosmos65/foco/-/raw/main/data/indicadores/acesso_conhecimento.csv',sep=";")
            
                st.table(df.style.highlight_max(color="red"))

            elif visIndicadores == '5 Piores':
                df = pd.read_csv('https://gitlab.com/Cosmos65/foco/-/raw/main/data/indicadores/acesso_conhecimento.csv',sep=";")
                
                col1, col2, col3, col4 = st.columns(4)

                with col1:
                    mortalidade_na_infancia = df[['Região \nAdministrativa','Alfabetização']].sort_values(by='Alfabetização').tail()

                    st.table(mortalidade_na_infancia)
                with col2:
                    baixo_peso_ao_nascer = df[['Região \nAdministrativa','Qualidade do Ensino Fundamental, anos iniciais']].sort_values(by='Qualidade do Ensino Fundamental, anos iniciais').tail()
                    
                    st.table(baixo_peso_ao_nascer)
                with col3:
                    mortalidade_materna = df[['Região \nAdministrativa','Qualidade do Ensino Fundamental, anos finais']].sort_values(by='Qualidade do Ensino Fundamental, anos finais').tail()

                    st.table(mortalidade_materna)

                with col4:
                    internacoes_criseRespAguda = df[['Região \nAdministrativa','Abandono escolar no Ensino Médio']].sort_values(by='Abandono escolar no Ensino Médio').tail()

                    st.table(internacoes_criseRespAguda)

    elif mapa == "Saúde e Bem-Estar":
        m = leafmap.Map(google_maps='ROADMAP',center=[0,0],zoom=2)

        in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/ips_saudeBemEstar.json"
        m.add_data(in_geojson,column='Saúde e bem-estar',scheme='EqualInterval',k=33, cmap='inferno',legend_title='IPS RA')

        m.to_streamlit(height=1000, width=2000)
        with st.container():
            st.title("Indicadores")

            #st = st.st("Veja os indicadores")
            
            visIndicadores = st.radio('Escolha a forma de visualização dos indicadores',('5 Piores','Piores Indicadores'))

            if visIndicadores == 'Piores Indicadores':
                df = pd.read_csv('https://gitlab.com/Cosmos65/foco/-/raw/main/data/indicadores/saude_bem_estar.csv',sep=";")
            
                st.table(df.style.highlight_max(color="red"))

            elif visIndicadores == '5 Piores':
                df = pd.read_csv('https://gitlab.com/Cosmos65/foco/-/raw/main/data/indicadores/saude_bem_estar.csv',sep=";")
                
                col1, col2, col3 = st.columns(3)

                with col1:
                    mortalidade_na_infancia = df[['Região \nAdministrativa','Mortalidade por doenças crônicas']].sort_values(by='Mortalidade por doenças crônicas').tail()

                    st.table(mortalidade_na_infancia)
                with col2:
                    baixo_peso_ao_nascer = df[['Região \nAdministrativa','Incidência de dengue']].sort_values(by='Incidência de dengue').tail()
                    
                    st.table(baixo_peso_ao_nascer)
                with col3:
                    mortalidade_materna = df[['Região \nAdministrativa','Mortalidade por tuberculose e HIV']].sort_values(by='Mortalidade por tuberculose e HIV').tail()

                    st.table(mortalidade_materna)

            
    elif mapa == "Qualidade do Meio Ambiente":
        m = leafmap.Map(google_maps='ROADMAP',center=[0,0],zoom=2)

        in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/ips_qualMeioAmbiente.json"
        m.add_data(in_geojson,column='Qualidade do meio ambiente',scheme='EqualInterval',k=33, cmap='inferno',legend_title='IPS RA')

        m.to_streamlit(height=1000, width=2000)
        with st.container():
            st.title("Indicadores")

            #st = st.st("Veja os indicadores")
            
            visIndicadores = st.radio('Escolha a forma de visualização dos indicadores',('5 Piores','Piores Indicadores'))

            if visIndicadores == 'Piores Indicadores':
                df = pd.read_csv('https://gitlab.com/Cosmos65/foco/-/raw/main/data/indicadores/qualidade_meio_ambiente.csv',sep=";")
            
                st.table(df.style.highlight_max(color="red"))

            elif visIndicadores == '5 Piores':
                df = pd.read_csv('https://gitlab.com/Cosmos65/foco/-/raw/main/data/indicadores/qualidade_meio_ambiente.csv',sep=";")
                
                col1, col2, col3, col4 = st.columns(4)

                with col1:
                    mortalidade_na_infancia = df[['Região \nAdministrativa','Coleta seletiva de lixo']].sort_values(by='Coleta seletiva de lixo').tail()

                    st.table(mortalidade_na_infancia)
                with col2:
                    baixo_peso_ao_nascer = df[['Região \nAdministrativa','Degradação de áreas verdes']].sort_values(by='Degradação de áreas verdes').tail()
                    
                    st.table(baixo_peso_ao_nascer)
 
    elif mapa == "Direitos Individuais":
        m = leafmap.Map(google_maps='ROADMAP',center=[0,0],zoom=2)

        in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/ips_direitosInividuais.json"
        m.add_data(in_geojson,column='Direitos individuais',scheme='EqualInterval',k=33, cmap='inferno',legend_title='IPS RA')

        m.to_streamlit(height=1000, width=2000)
        with st.container():
            st.title("Indicadores")

            #st = st.st("Veja os indicadores")
            
            visIndicadores = st.radio('Escolha a forma de visualização dos indicadores',('5 Piores','Piores Indicadores'))

            if visIndicadores == 'Piores Indicadores':
                df = pd.read_csv('https://gitlab.com/Cosmos65/foco/-/raw/main/data/indicadores/direitos_individuais.csv',sep=";")
            
                st.table(df.style.highlight_max(color="red"))

            elif visIndicadores == '5 Piores':
                df = pd.read_csv('https://gitlab.com/Cosmos65/foco/-/raw/main/data/indicadores/direitos_individuais.csv',sep=";")
                
                col1, col2, col3, col4 = st.columns(4)

                with col1:
                    mortalidade_na_infancia = df[['Região \nAdministrativa','Mobilidade urbana']].sort_values(by='Mobilidade urbana').tail()

                    st.table(mortalidade_na_infancia)
                with col2:
                    baixo_peso_ao_nascer = df[['Região \nAdministrativa','Homicídios por ação policial']].sort_values(by='Homicídios por ação policial').tail()
                    
                    st.table(baixo_peso_ao_nascer)
                with col3:
                    mortalidade_materna = df[['Região \nAdministrativa','Tempo médio de deslocamento']].sort_values(by='Tempo médio de deslocamento').tail()

                    st.table(mortalidade_materna)

                with col4:
                    internacoes_criseRespAguda = df[['Região \nAdministrativa','Participação política']].sort_values(by='Participação política').tail()

                    st.table(internacoes_criseRespAguda)
    elif mapa == "Liberdades Individuais":
        m = leafmap.Map(google_maps='ROADMAP',center=[0,0],zoom=2)

        in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/ips_liberdadesIndividuais.json"
        m.add_data(in_geojson,column='Liberdades individuais',scheme='EqualInterval',k=33, cmap='inferno',legend_title='IPS RA')

        m.to_streamlit(height=1000, width=2000)
        with st.container():
            st.title("Indicadores")

            #st = st.st("Veja os indicadores")
            
            visIndicadores = st.radio('Escolha a forma de visualização dos indicadores',('5 Piores','Piores Indicadores'))

            if visIndicadores == 'Piores Indicadores':
                df = pd.read_csv('https://gitlab.com/Cosmos65/foco/-/raw/main/data/indicadores/liberdades_individuais.csv',sep=";")
            
                st.table(df.style.highlight_max(color="red"))

            elif visIndicadores == '5 Piores':
                df = pd.read_csv('https://gitlab.com/Cosmos65/foco/-/raw/main/data/indicadores/liberdades_individuais.csv',sep=";")
                
                col1, col2, col3 = st.columns(3)

                with col1:
                    mortalidade_na_infancia = df[['Região \nAdministrativa','Gravidez na adolescência']].sort_values(by='Gravidez na adolescência').tail()

                    st.table(mortalidade_na_infancia)
                with col2:
                    baixo_peso_ao_nascer = df[['Região \nAdministrativa','Trabalho infantil']].sort_values(by='Trabalho infantil').tail()
                    
                    st.table(baixo_peso_ao_nascer)
                with col3:
                    mortalidade_materna = df[['Região \nAdministrativa','Índice de acesso à cultura']].sort_values(by='Índice de acesso à cultura').tail()

                    st.table(mortalidade_materna)

    elif mapa == "Tolerância e Inclusão":
        m = leafmap.Map(google_maps='ROADMAP',center=[0,0],zoom=2)

        in_geojson = "https://gitlab.com/Cosmos65/foco/-/blob/main/data/geoJson/ips_toleranciaInclusao.json"
        m.add_data(in_geojson,column='Tolerância e inclusão',scheme='EqualInterval',k=33, cmap='inferno',legend_title='IPS RA')

        m.to_streamlit(height=1000, width=2000)
        with st.container():
            st.title("Indicadores")

            #st = st.st("Veja os indicadores")
            
            visIndicadores = st.radio('Escolha a forma de visualização dos indicadores',('5 Piores','Piores Indicadores'))

            if visIndicadores == 'Piores Indicadores':
                df = pd.read_csv('https://gitlab.com/Cosmos65/foco/-/raw/main/data/indicadores/tolerancia_inclusao.csv',sep=";")
            
                st.table(df.style.highlight_max(color="red"))

            elif visIndicadores == '5 Piores':
                df = pd.read_csv('https://gitlab.com/Cosmos65/foco/-/raw/main/data/indicadores/tolerancia_inclusao.csv',sep=";")
                
                col1, col2, col3 = st.columns(3)

                with col1:
                    mortalidade_na_infancia = df[['Região \nAdministrativa','Violência contra a mulher']].sort_values(by='Violência contra a mulher').tail()

                    st.table(mortalidade_na_infancia)
                with col2:
                    baixo_peso_ao_nascer = df[['Região \nAdministrativa','Homicídios de jovens negros']].sort_values(by='Homicídios de jovens negros').tail()
                    
                    st.table(baixo_peso_ao_nascer)
                with col3:
                    mortalidade_materna = df[['Região \nAdministrativa','Vulnerabilidade familiar']].sort_values(by='Vulnerabilidade familiar').tail()

                    st.table(mortalidade_materna)

            
    elif mapa == "Acesso á Educação Superior":
        m = leafmap.Map(google_maps='ROADMAP',center=[0,0],zoom=2)

        in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/ips_acessoEducacaoSuperior.json"
        m.add_data(in_geojson,column='Acesso á educação superior',scheme='EqualInterval',k=33, cmap='inferno',legend_title='IPS RA')

        m.to_streamlit(height=1000, width=2000)
        with st.container():
            st.title("Indicadores")

            #st = st.st("Veja os indicadores")
            
            visIndicadores = st.radio('Escolha a forma de visualização dos indicadores',('5 Piores','Piores Indicadores'))

            if visIndicadores == 'Piores Indicadores':
                df = pd.read_csv('https://gitlab.com/Cosmos65/foco/-/raw/main/data/indicadores/acesso_educacao_superior.csv',sep=";")
            
                st.table(df.style.highlight_max(color="red"))

            elif visIndicadores == '5 Piores':
                df = pd.read_csv('https://gitlab.com/Cosmos65/foco/-/raw/main/data/indicadores/acesso_educacao_superior.csv',sep=";")
                
                col1, col2, col3 = st.columns(3)

                with col1:
                    mortalidade_na_infancia = df[['Região \nAdministrativa','Pessoas com Ensino Superior']].sort_values(by='Pessoas com Ensino Superior').tail()

                    st.table(mortalidade_na_infancia)
                with col2:
                    baixo_peso_ao_nascer = df[['Região \nAdministrativa','Negros e indígenas com Ensino Superior']].sort_values(by='Negros e indígenas com Ensino Superior').tail()
                    
                    st.table(baixo_peso_ao_nascer)
                with col3:
                    mortalidade_materna = df[['Região \nAdministrativa','Frequência ao Ensino Superior']].sort_values(by='Frequência ao Ensino Superior').tail()

                    st.table(mortalidade_materna)

    elif mapa == "Acesso à Informação":
        m = leafmap.Map(google_maps='ROADMAP',center=[0,0],zoom=2)

        in_geojson = "https://gitlab.com/Cosmos65/foco/-/raw/main/data/geoJson/ips_acessoInformacao.json"
        m.add_data(in_geojson,column='Acesso à informação',scheme='EqualInterval',k=33, cmap='inferno',legend_title='IPS RA')

        m.to_streamlit(height=1000, width=2000)
        
        with st.container():

            st.title("Indicadores")

            #st = st.st("Veja os indicadores")
            
            visIndicadores = st.radio('Escolha a forma de visualização dos indicadores',('5 Piores','Piores Indicadores'))

            if visIndicadores == 'Piores Indicadores':
                df = pd.read_csv('https://gitlab.com/Cosmos65/foco/-/raw/main/data/indicadores/acesso_informacao.csv',sep=";")
            
                st.table(df.style.highlight_max(color="red"))

            elif visIndicadores == '5 Piores':
                df = pd.read_csv('https://gitlab.com/Cosmos65/foco/-/raw/main/data/indicadores/acesso_informacao.csv',sep=";")
                
                col1, col2 = st.columns(2)

                with col1:
                    mortalidade_na_infancia = df[['Região \nAdministrativa','Acesso à telefone celular ou fixo']].sort_values(by='Acesso à telefone celular ou fixo').tail()

                    st.table(mortalidade_na_infancia)
                with col2:
                    baixo_peso_ao_nascer = df[['Região \nAdministrativa','Acesso a internet']].sort_values(by='Acesso a internet').tail()
                    
                    st.table(baixo_peso_ao_nascer)
#    m = leafmap.Map(google_maps='ROADMAP',center=[0,0],zoom=2)
#
#    in_geojson = "/home/leolo/Área de Trabalho/labfaz/projetos/foco/.venv/foco/data/gdf_ips_2020.json"
#    m.add_data(in_geojson,column='ips',scheme='EqualInterval',k=33, cmap='inferno',legend_title='IPS RA')

#    m.to_streamlit(height=1000, width=2000)


