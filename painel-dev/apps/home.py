import streamlit as st
from PIL import Image
def app():
    st.title("Ferramenta Otimizadora de Custo Oportunidade")

    st.subheader('A Ferramenta de Otimização Custo Oportunidade (FOCO) é uma tecnologia, desenvolvida pelo Lab.Faz, que simula e diagnostica investimentos públicos na Cidade do Rio de Janeiro. Trata-se de uma instrumentalização do Índice de Progresso Social (IPS), sob o propósito de orientar decisões da alta gestão.')
    #st.header('Qualidade:')
    st.subheader('Para tanto, o FOCO abarca uma metodologia que integra, a partir da ideia de custo oportunidade, localidades urbanas por meio das 33 variáveis que fundamentam o IPS. A ideia de custo oportunidade pressupõe que determinado investimento em uma região significa o não investimento em todo o resto da cidade. É um ferramental que pode ser pensado enquanto uma balança, que mede o equilíbrio entre os recursos escassos disponíveis à prefeitura.')
    #st.header('Temporal:')
    st.subheader('Nesse sentido, a análise de uma região deixa de ser atomizada para ser situada em meio a um contexto de cidade. O papel do IPS no FOCO é o de oferecer – a partir de suas trinta e três variáveis – um repertório de análise e de ação. O mecanismo dessa balança é desenhado nos critérios que relacionam os impactos de cada variável. Para entender seu funcionamento é necessário dimensionarmos o IPS em sua forma customizada à cidade do Rio de Janeiro.')
    #st.header('Territorial:')
    #st.subheader('Sob esta perspectiva é possível georeferenciar a dívida em escalas de bairro, região administrativa e área de planejamento. Ao assumir essas três perspectivas, cria-se um universo de customização de informações. Ou seja, é possível inspirar e responder perguntas que cruzam valor, tempo e território.')

    col1, col2 = st.columns(2)

    with col1:
        image1 = Image.open('/home/leolo/Área de Trabalho/labfaz/projetos/foco/.venv/foco/data/RIOPREFEITURA FundaçãoJoãoGoulart horizontal branco.png')
        st.image(image1)

    with col2:
        image2 = Image.open('/home/leolo/Área de Trabalho/labfaz/projetos/foco/.venv/foco/data/logo_labfaz_page-0001-removebg-preview.png')
        st.image(image2)

    